document.addEventListener("DOMContentLoaded", function () {
  const mobMenuHandler = function () {
    let mobMenuBtn = document.getElementById("mobBtn");
    let mobMenuLinks = document.querySelectorAll(".mob-nav__link");
    let mobMenu = document.getElementById("mobMenuNav");
    // let contactsLink = document.getElementById("contactsMobLink");

    // contactsLink.addEventListener("click", function (e) {
    //   let contactsBlock = document.getElementById("contacts");
    //   e.preventDefault();
    //   mobMenuBtn.classList.remove("-active");
    //   document.body.classList.remove("-mob-menu");
    //   contactsBlock.scrollIntoView({ behavior: "smooth" });
    // });

    mobMenuBtn.addEventListener("click", mobMenuToggle);

    mobMenuLinks.forEach((element) => {
      element.addEventListener("click", mobSubmenuShow);
    });

    function mobMenuToggle() {
      if (mobMenuBtn.classList.contains("-active")) {
        mobMenu.style.height = "auto";
        mobMenuLinks.forEach((element) => {
          element.classList.remove("-active");
        });
      }

      mobMenuBtn.classList.toggle("-active");
      document.body.classList.toggle("-mob-menu");
    }

    function mobSubmenuShow(event) {
      if (this.nextElementSibling && this.nextElementSibling.className === "mob-menu__subnav") {
        event.preventDefault();
        let activeSubmenuLink = this;
        let subMenuHeight = this.nextElementSibling.offsetHeight;
        let subMenuCloseBtn = this.nextElementSibling.querySelector(".mob-nav__item.-back");
        subMenuCloseBtn.addEventListener("click", function () {
          activeSubmenuLink.classList.remove("-active");
          mobMenuLinks.forEach((element) => {
            element.classList.remove("-active");
          });
          mobMenu.style.height = "auto";
        });
        this.classList.toggle("-active");
        mobMenu.style.height = subMenuHeight + "px";
      } else if (this.nextElementSibling && this.nextElementSibling.className === "subnav__list") {
        event.preventDefault();
        let upperSubMenu = this.closest(".mob-menu__subnav");
        this.classList.toggle("-active");
        mobMenu.style.height = upperSubMenu.offsetHeight + "px";
      }
      return false;
    }
  };

  const toTopHandler = function () {
    let toTopButton = document.getElementById("toTop");
    toTopButton.addEventListener("click", function (event) {
      event.preventDefault();
      window.scroll({ top: 0, left: 0, behavior: "smooth" });
    });
  };

  // const toContactsHandler = function () {
  //   let contactsButton = document.querySelector(".header-menu__link[href='#contacts']");
  //   let contactsBlock = document.getElementById("contacts");
  //   contactsButton.addEventListener("click", function (event) {
  //     event.preventDefault();
  //     contactsBlock.scrollIntoView({ behavior: "smooth" });
  //   });
  // };

  const desktopMenuHandler = function () {
    let headerSubmenu = document.querySelectorAll(".header__submenu");
    headerSubmenu.forEach((element) => {
      element.addEventListener("mouseenter", function (event) {
        element.previousElementSibling.classList.add("hovered");
      });
      element.addEventListener("mouseleave", function (event) {
        element.previousElementSibling.classList.remove("hovered");
      });
    });
  };

  mobMenuHandler();
  toTopHandler();
  // toContactsHandler();
  desktopMenuHandler();

  if (document.getElementsByClassName("svg-inject").length) {
    SVGInject(document.getElementsByClassName("map__img"), {
      onAllFinish: function mapPulse() {
        let mapDots = gsap.utils.toArray(document.querySelectorAll(".map__dot"));
        let randomGroup = gsap.utils.shuffle(mapDots, true).slice(0, gsap.utils.random(10, 40));
        let mapPulseTimeline = gsap.timeline({ onComplete: mapPulse });
        mapPulseTimeline.set(randomGroup, { transformOrigin: "50% 50%" });
        mapPulseTimeline.to(randomGroup, {
          transformOrigin: "50% 50%",
          scale: 1.2,
          duration: 2,
          fill: "#e51a4b",
          ease: Power2.easeInOut,
          repeat: 1,
          yoyo: true,
        });
      },
    });

    SVGInject(document.getElementsByClassName("slide-bg__img"), {
      makeIdsUnique: false,
      onAllFinish: function () {
        if (document.getElementById("mainSlider")) {
          let splideHero = new Splide("#mainSlider", {
            type: "fade",
            arrows: false,
            speed: 100,
            rewind: true,
            // autoplay: true
          });
          let progressDot = document.getElementById("progressDot");
          let nextButton = document.querySelectorAll(".slide__arrow");
          let slidesContainer = document.getElementById("sliderBg");
          let sliderBgImages = document.querySelectorAll(".slide-bg__img");

          const nextSlide = function (event) {
            splideHero.go("+");
          };

          let slideTimelines = [];
          for (let i = 0; i < sliderBgImages.length; i++) {
            let timelineSlide;
            switch (i) {
              case 0:
                timelineSlide = gsap.timeline({ paused: true, delay: 0 });
                timelineSlide
                  .fromTo(".slide-stroke-circle", { opacity: 0 }, { opacity: 1, duration: 1.5, stagger: -0.04 })
                  .set("#slideOneCar", { scaleY: -1, scaleX: -1 }, 0)
                  .from(
                    "#slideOneCar",
                    {
                      repeat: 0,
                      duration: 20,
                      ease: "none",
                      // onComplete: nextSlide,
                      motionPath: {
                        path: "#slideOnePath",
                        align: "#slideOnePath",
                        autoRotate: true,
                        alignOrigin: [1, 0.9],
                      },
                    },
                    0.7
                  )
                  .fromTo("#progressDot", { left: "0%" }, { left: "100%", duration: 11, ease: "none" }, 0.7)
                  .fromTo(".slide-stroke-circle", { rotation: 0 }, { rotation: -360, transformOrigin: "50% 50%", duration: 25, repeat: -1, ease: "none" }, 0)
                  .fromTo(".slide-grid__left", { opacity: 0, x: -100 }, { duration: 0.6, opacity: 1, x: 0 }, 0)
                  .fromTo(".slide-grid__right", { opacity: 0, x: 100 }, { duration: 0.6, opacity: 1, x: 0 }, 0)
                  .fromTo(".slide-grid__center", { opacity: 0, y: -100 }, { duration: 0.6, ease: "power1.inOut", opacity: 1, y: 0 }, 0)
                  .call(nextSlide, null, 13);
                break;
              case 1:
                timelineSlide = gsap.timeline({ paused: true });
                timelineSlide
                  .fromTo(".slide-stroke-wave", { strokeDashoffset: 1 }, { strokeDashoffset: 0, ease: Back, duration: 4, stagger: 0.3 })
                  .set("#slideTwoCar", { y: 200, transformOrigin: "50% 50%" }, 0)
                  .to(
                    "#slideTwoCar",
                    {
                      repeat: -1,
                      duration: 15,
                      ease: "none",
                      motionPath: {
                        path: "#slideTwoPath",
                        align: "#slideTwoPath",
                        curviness: 2,
                        autoRotate: true,
                        start: 0,
                        end: 1,
                        alignOrigin: [0.7, 1],
                      },
                    },
                    0
                  )
                  .fromTo("#progressDot", { left: "0%" }, { left: "100%", duration: 11, ease: "none" }, 0.7)
                  .fromTo(".slide-grid__left", { opacity: 0, x: -100 }, { duration: 0.6, opacity: 1, x: 0 }, 0)
                  .fromTo(".slide-grid__right", { opacity: 0, x: 100 }, { duration: 0.6, opacity: 1, x: 0 }, 0)
                  .fromTo(".slide-grid__center", { opacity: 0, y: -100 }, { duration: 0.6, ease: "power1.inOut", opacity: 1, y: 0 }, 0)
                  .fromTo(".slide-stroke-dash", { strokeDashoffset: 0 }, { strokeDashoffset: -8, duration: 0.4, repeat: -1, ease: "none" }, 0)
                  .call(nextSlide, null, 13);
                break;
              case 2:
                timelineSlide = gsap.timeline({ paused: true });
                timelineSlide
                  .fromTo(
                    ".slide-stroke-hor",
                    { strokeDashoffset: 1 },
                    {
                      strokeDashoffset: 0,
                      ease: "none",
                      duration: 4,
                      stagger: 0.05,
                    },
                    0.5
                  )
                  .to(
                    "#slideThreeCar",
                    {
                      repeat: -1,
                      duration: 20,
                      ease: "none",
                      motionPath: {
                        path: "#slideThreePathHor",
                        align: "#slideThreePathHor",
                        curviness: 2,
                        autoRotate: true,
                        start: 0,
                        end: 1,
                        alignOrigin: [0.5, 1],
                      },
                    },
                    1
                  )
                  .fromTo("#progressDot", { left: "0%" }, { left: "100%", duration: 14, ease: "none" }, 0.7)
                  .fromTo(".slide-stroke-vert", { strokeDashoffset: 1 }, { strokeDashoffset: 0, ease: "back.inOut(1)", duration: 4, stagger: 0 }, 0)
                  .fromTo(".slide-grid__left", { opacity: 0, x: -100 }, { duration: 0.6, opacity: 1, x: 0 }, 0)
                  .fromTo(".slide-grid__right", { opacity: 0, x: 100 }, { duration: 0.6, opacity: 1, x: 0 }, 0)
                  .fromTo(".slide-grid__center", { opacity: 0, y: -100 }, { duration: 0.6, ease: "power1.inOut", opacity: 1, y: 0 }, 0)
                  .call(nextSlide, null, 16);
                break;
              default:
                break;
            }
            slideTimelines.push(timelineSlide);
          }

          splideHero.on("mounted", function () {
            slidesContainer.classList.add("active-1");
            slideTimelines[0].resume();
          });

          splideHero.on("move", function (newIndex, oldIndex) {
            // let progressWidth = Math.floor((newIndex / (slidersLength - 1)) * 100);
            // progressDot.style.left = progressWidth + "%";
            slideTimelines.forEach((timeline) => {
              timeline.seek(0);
              timeline.pause();
            });
            slidesContainer.classList.remove(`active-${oldIndex + 1}`);
            slidesContainer.classList.add(`active-${newIndex + 1}`);
            sliderBgImages.forEach((element) => {
              element.classList.remove("active");
            });
            sliderBgImages[newIndex].classList.add("active");
            slideTimelines[newIndex].resume();
          });

          splideHero.mount();

          let slidersLength = splideHero.length;

          const goNextButton = function (event) {
            event.preventDefault();
            splideHero.go("+");
          };

          nextButton.forEach((element) => {
            element.addEventListener("click", goNextButton);
          });
        }
      },
    });
  }

  // SLIDERS

  if (document.getElementById("gallerySlider")) {
    let splideGallery = new Splide("#gallerySlider", {
      perPage: 1,
      perMove: 1,
      autoWidth: true,
      gap: 40,
      trimSpace: false,
      height: 280,
      focus: "center",
      breakpoints: {
        767: {
          perPage: 1,
          autoWidth: false,
          padding: 20,
        },
      },
    }).mount();
  }

  if (document.getElementById("historySlider")) {
    let sliderWidth = document.getElementsByClassName("splide__track")[0].offsetWidth;

    let splideHistory = new Splide("#historySlider", {
      fixedWidth: sliderWidth / 4 - 18 + "px",
      autoWidth: false,
      gap: 24,
      trimSpace: false,
      updateOnMove: true,
      perMove: 2,
      perPage: 2,
      breakpoints: {
        1250: {
          fixedWidth: sliderWidth / 3 - 16 + "px",
        },
        767: {
          fixedWidth: 0,
          perPage: 1,
          perMove: 1,
          focus: "center",
          autoWidth: false,
          padding: 20,
          gap: 40,
        },
      },
    }).mount();
  }

  if (document.getElementById("advantagesSlider")) {
    let splideAdvantages = new Splide("#advantagesSlider", {
      perPage: 4,
      gap: 24,
      drag: false,
      pagination: false,
      breakpoints: {
        1250: {
          perPage: 3,
          drag: true,
          pagination: true,
        },
        767: {
          perPage: 1,
          drag: true,
          pagination: true,
        },
      },
    }).mount();
  }

  // SLIDERS END

  // TABS
  if (document.getElementsByClassName("tabs").length) {
    let tabs = document.querySelectorAll(".tabs__item");
    let tabPrev = document.getElementById("tabPrev");
    let tabNext = document.getElementById("tabNext");

    tabPrev.addEventListener("click", function () {
      let currentPanel = document.querySelector(".panel.-active");
      let currentPanelId = currentPanel.dataset.tabPanel;
      if (currentPanel.dataset.tabPanel > 1) {
        currentPanelId--;
        document.querySelector(".panel.-active").classList.remove("-active");
        document.querySelector(".tabs__item.-active").classList.remove("-active");
        document.querySelector(`.panel[data-tab-panel="${currentPanelId}"]`).classList.add("-active");
        document.querySelector(`.tabs__item[data-tab="${currentPanelId}"]`).classList.add("-active");
      }
      if (currentPanelId == 1) {
        tabPrev.classList.remove("-disabled");
        tabNext.classList.remove("-disabled");
        this.classList.add("-disabled");
      } else {
        tabPrev.classList.remove("-disabled");
        tabNext.classList.remove("-disabled");
      }
      return false;
    });
    tabNext.addEventListener("click", function () {
      let currentPanel = document.querySelector(".panel.-active");
      let currentPanelId = currentPanel.dataset.tabPanel;
      if (currentPanel.dataset.tabPanel < tabs.length) {
        currentPanelId++;
        document.querySelector(".panel.-active").classList.remove("-active");
        document.querySelector(".tabs__item.-active").classList.remove("-active");
        document.querySelector(`.panel[data-tab-panel="${currentPanelId}"]`).classList.add("-active");
        document.querySelector(`.tabs__item[data-tab="${currentPanelId}"]`).classList.add("-active");
      }
      if (currentPanelId == tabs.length) {
        tabPrev.classList.remove("-disabled");
        tabNext.classList.remove("-disabled");
        this.classList.add("-disabled");
      } else {
        tabNext.classList.remove("-disabled");
        tabPrev.classList.remove("-disabled");
      }
      return false;
    });

    tabs.forEach((tab) => {
      tab.addEventListener("click", function () {
        let currentPanel = document.querySelector('.panel[data-tab-panel="' + this.dataset.tab + '"]');
        document.querySelector(".panel.-active").classList.remove("-active");
        document.querySelector(".tabs__item.-active").classList.remove("-active");
        currentPanel.classList.add("-active");
        this.classList.add("-active");
        if (this.dataset.tab == 1) {
          tabPrev.classList.remove("-disabled");
          tabNext.classList.remove("-disabled");
          tabPrev.classList.add("-disabled");
        } else if (this.dataset.tab == tabs.length) {
          tabPrev.classList.remove("-disabled");
          tabNext.classList.remove("-disabled");
          tabNext.classList.add("-disabled");
        } else {
          tabPrev.classList.remove("-disabled");
          tabNext.classList.remove("-disabled");
        }
      });
    });
  }

  // TABS END

  // SCROLL SECTIONS FADE
  if (document.querySelectorAll(".hero-slide__inner.-advantages").length) {
    let fadeHeroAdvantages = gsap.timeline({
      scrollTrigger: {
        trigger: ".hero-slide__inner.-advantages",
        start: "top+=200px bottom",
        duration: 0.7,
        ease: CustomEase.create("cubic-bezier", ".19,1,.22,1"),
        markers: false,
      },
    });
    fadeHeroAdvantages
      .fromTo(
        ".hero-slide__inner.-advantages .heading-three",
        { x: -100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
        }
      )
      .fromTo(".hero-slide__inner.-advantages .splide__track", { y: 100, opacity: 0 }, { y: 0, opacity: 1 });
  }

  if (document.querySelectorAll(".hero-slide__inner.-map").length) {
    let fadeHeroMap = gsap.timeline({
      scrollTrigger: {
        trigger: ".hero-slide__inner.-map",
        start: "top+=200px bottom",
        duration: 0.7,
        ease: "cubic-bezier",
        markers: false,
      },
    });
    fadeHeroMap
      .fromTo(
        ".hero-slide__inner.-map .heading-three",
        { x: -100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
        }
      )
      .fromTo(".map__all, .map, .map__belarus", { y: 100, opacity: 0 }, { y: 0, opacity: 1 })
      .set(".solutions__item:nth-child(odd)", { x: 100, opacity: 0 })
      .set(".solutions__item:nth-child(even)", { x: -100, opacity: 0 });
  }

  if (document.querySelectorAll(".hero-slide__inner.-solutions").length) {
    let fadeHeroSolutions = gsap.timeline({
      scrollTrigger: {
        trigger: ".hero-slide__inner.-solutions",
        start: "top+=200px bottom",
        duration: 0.7,
        ease: "cubic-bezier",
        markers: false,
      },
    });
    fadeHeroSolutions
      .fromTo(
        ".hero-slide__inner.-solutions .heading-three",
        { x: -100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
        }
      )
      .to(".solutions__item", { x: 0, opacity: 1, stagger: 0.3 });
  }

  if (document.querySelector(".title-section__grid")) {
    let fadeInnerPageTop = gsap.timeline({
      scrollTrigger: {
        trigger: ".title-section__grid",
        start: "top+=200px bottom",
        duration: 0.7,
        ease: "cubic-bezier",
        markers: false,
      },
    });
    fadeInnerPageTop
      .fromTo(
        ".title-section__grid .heading-one",
        { x: -100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
        }
      )
      .fromTo(
        ".title-section__grid .btn__play",
        { x: 100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
        }
      )
      .fromTo(".inner__line.-title", { bottom: "110%" }, { bottom: "20%", duration: 1 });
  }

  if (document.querySelector(".info-section__grid")) {
    let fadeInnerPageInfo = gsap.timeline({
      scrollTrigger: {
        trigger: ".info-section__grid",
        start: "top+=200px bottom",
        duration: 0.7,
        ease: "cubic-bezier",
        markers: false,
      },
    });
    fadeInnerPageInfo
      .fromTo(
        ".info-section__grid .heading-two",
        { x: -100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
        },
        0.7
      )
      .fromTo(
        ".info-section__grid .info-section__left",
        { x: -100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
        }
      )
      .fromTo(
        ".info-section__grid .info-section__right",
        { x: 100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
        }
      );
  }

  if (document.querySelector(".items-section__grid.-equipment")) {
    let getDistanceToTitle = function () {
      return document.querySelector(".heading-one").getBoundingClientRect().left - 20;
    };
    let fadeInnerEquipmentItems = gsap.timeline({
      scrollTrigger: {
        trigger: ".items-section__grid.-equipment",
        start: "top+=200px bottom",
        ease: "cubic-bezier",
        markers: false,
      },
    });
    fadeInnerEquipmentItems
      .fromTo(
        ".items-section__grid.-equipment .heading-three",
        { x: -100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
          duration: 0.7,
        }
      )
      .fromTo(
        ".items-section__bg",
        { opacity: 0 },
        {
          opacity: 1,
          duration: 0.7,
        },
        0
      )
      .fromTo(
        ".items-section__grid.-equipment .equipment__item",
        { y: 100, opacity: 0 },
        {
          y: 0,
          opacity: 1,
          duration: 0.7,
        }
      )
      .fromTo(
        ".inner__line.-items",
        { width: 0 },
        {
          width: getDistanceToTitle,
          duration: 1.7,
        }
      );
  }

  if (document.querySelector(".items-section__grid .item")) {
    let getDistanceToTitle = function () {
      return document.querySelector(".heading-one").getBoundingClientRect().left - 20;
    };
    let fadeInnerSolutionsItems = gsap.timeline({
      scrollTrigger: {
        trigger: ".items-section__grid",
        start: "top+=200px bottom",
        ease: "cubic-bezier",
        markers: false,
      },
    });
    fadeInnerSolutionsItems
      .fromTo(
        ".items-section__grid .heading-three",
        { x: -100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
          duration: 0.7,
        }
      )
      .fromTo(
        ".items-section__bg",
        { opacity: 0 },
        {
          opacity: 1,
          duration: 0.7,
        },
        0
      )
      .fromTo(
        ".items-section__grid .item",
        { y: 100, opacity: 0 },
        {
          y: 0,
          opacity: 1,
          duration: 0.7,
        }
      )
      .fromTo(
        ".inner__line.-items",
        { width: 0 },
        {
          width: getDistanceToTitle,
          duration: 1.7,
        }
      );
  }

  if (document.querySelector(".solutions__title")) {
    let fadeSolutionsTitle = gsap.timeline({
      scrollTrigger: {
        trigger: ".solutions__title",
        start: "top+=200px bottom",
        ease: "cubic-bezier",
        markers: false,
      },
    });
    fadeSolutionsTitle
      .fromTo(
        ".solutions__title .heading-one",
        { x: -100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
          duration: 0.6,
        }
      )
      .fromTo(
        ".solutions__title .btn__play",
        { x: 100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
          duration: 0.6,
        }
      )
      .fromTo(
        ".solutions__title .solutions-title__icon",
        { y: -100, opacity: 0 },
        {
          y: 0,
          opacity: 1,
          duration: 0.6,
        }
      )
      .fromTo(
        ".solutions__top .slide-stroke-hor",
        { strokeDashoffset: 1 },
        {
          strokeDashoffset: 0,
          ease: "none",
          duration: 4,
          stagger: 0.05,
        },
        0.5
      );
  }

  if (document.querySelector(".solutions__info")) {
    let fadeSolutionsInfo = gsap.timeline({
      scrollTrigger: {
        trigger: ".solutions__info",
        start: "top+=200px bottom",
        ease: "cubic-bezier",
        markers: false,
      },
    });
    fadeSolutionsInfo
      .fromTo(
        ".solutions__info .heading-two",
        { x: -100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
          duration: 0.7,
        },
        0.5
      )
      .fromTo(
        ".solutions__info .solutions-info__left",
        { x: -100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
          duration: 0.7,
        },
        1
      )
      .fromTo(
        ".solutions__info .solutions-info__right",
        { x: 100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
          duration: 0.7,
        },
        1
      );
  }

  if (document.querySelector(".solution__points")) {
    let fadeSolutionsPoints = gsap.timeline({
      scrollTrigger: {
        trigger: ".solution__points",
        start: "top+=200px bottom",
        ease: "cubic-bezier",
        markers: false,
      },
    });
    fadeSolutionsPoints
      .fromTo(
        ".solution__points .heading-two",
        { x: -100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
          duration: 0.7,
        },
        0.5
      )
      .fromTo(
        ".solution__points .solution-point",
        { y: 100, opacity: 0 },
        {
          y: 0,
          opacity: 1,
          duration: 0.7,
        }
      );
  }

  if (document.querySelector(".form-section")) {
    let fadeInnerPageForm = gsap.timeline({
      scrollTrigger: {
        trigger: ".form-section",
        start: "top+=200px bottom",
        duration: 0.7,
        ease: "cubic-bezier",
        markers: false,
      },
    });
    fadeInnerPageForm
      .fromTo(
        ".form__background",
        { x: -100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
        }
      )
      .fromTo(
        "form.form",
        { x: 100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
        },
        0
      );
  }

  if (document.querySelector(".tabs-section")) {
    ScrollTrigger.matchMedia({
      "(min-width: 768px)": function () {
        let fadeInnerPageTabs = gsap.timeline({
          scrollTrigger: {
            trigger: ".tabs-section",
            start: "top+=200px bottom",
            duration: 0.7,
            ease: "cubic-bezier",
            markers: false,
          },
        });
        fadeInnerPageTabs
          .fromTo(
            ".tabs__item",
            { x: 100, opacity: 0 },
            {
              x: 0,
              opacity: 1,
              stagger: 0.3,
              duration: 0.3,
            }
          )
          .fromTo(
            ".panels",
            { y: 100, opacity: 0 },
            {
              y: 0,
              opacity: 1,
            },
            0
          );
      },
      "(max-width: 767px)": function () {
        let fadeInnerPageTabs = gsap.timeline({
          scrollTrigger: {
            trigger: ".tabs-section",
            start: "top+=200px bottom",
            duration: 0.7,
            ease: "cubic-bezier",
            markers: false,
          },
        });
        fadeInnerPageTabs
          .fromTo(
            ".tabs__item",
            { y: 100, opacity: 0 },
            {
              y: 0,
              opacity: 1,
              stagger: 0.3,
              duration: 0.3,
            }
          )
          .fromTo(
            ".panels",
            { y: 100, opacity: 0 },
            {
              y: 0,
              opacity: 1,
            },
            0
          );
      },
    });
  }

  if (document.querySelector(".equipment__title")) {
    let fadeEquipmentTitle = gsap.timeline({
      scrollTrigger: {
        trigger: ".equipment__title",
        start: "top+=200px bottom",
        duration: 0.7,
        ease: "cubic-bezier",
        markers: false,
      },
    });
    fadeEquipmentTitle
      .fromTo(
        ".equipment__title .heading-one",
        { x: -100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
          duration: 0.7,
        }
      )
      .fromTo(
        ".equipment__title .btn__play",
        { x: 100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
        }
      )
      .fromTo(
        ".equipment-info__main",
        { x: -100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
        }
      )
      .fromTo(
        ".equipment-info__picture",
        { x: 100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
        }
      )
      .fromTo(
        ".equipment-info__text",
        { y: 100, opacity: 0 },
        {
          y: 0,
          opacity: 1,
        }
      );
  }

  if (document.querySelector(".results__inner")) {
    let fadeResults = gsap.timeline({
      scrollTrigger: {
        trigger: ".results__inner",
        start: "top+=200px bottom",
        duration: 0.7,
        ease: "cubic-bezier",
        markers: false,
      },
    });
    fadeResults
      .fromTo(
        ".results__inner .heading-three",
        { x: -100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
          duration: 0.7,
        }
      )
      .fromTo(
        ".results__inner .results__item",
        { y: 100, opacity: 0 },
        {
          y: 0,
          opacity: 1,
        }
      );
  }

  if (document.querySelector(".about__title")) {
    let fadeAboutTitle = gsap.timeline({
      scrollTrigger: {
        trigger: ".about__title",
        start: "top+=200px bottom",
        duration: 0.7,
        ease: "cubic-bezier",
        markers: false,
      },
    });
    fadeAboutTitle
      .fromTo(
        ".about__title .heading-one",
        { x: -100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
          duration: 0.7,
        }
      )
      .fromTo(
        ".about__title .btn__play",
        { x: 100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
        }
      )
      .fromTo(
        ".about__title .about__text",
        { x: -100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
        }
      )
      .fromTo(
        ".about__title .about__logo",
        { x: 100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
        }
      )
      .fromTo(".about__title .slide-stroke-wave", { strokeDashoffset: 1 }, { strokeDashoffset: 0, ease: Back, duration: 3, stagger: 0.3 }, "<");
  }

  if (document.querySelector(".gallery__inner ")) {
    let fadeGallery = gsap.timeline({
      scrollTrigger: {
        trigger: ".gallery__inner ",
        start: "top+=200px bottom",
        duration: 0.7,
        ease: "cubic-bezier",
        markers: false,
      },
    });
    fadeGallery
      .fromTo(
        ".gallery__inner  .heading-three",
        { x: -100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
          duration: 0.7,
        }
      )
      .fromTo(
        ".gallery__inner  .splide__track",
        { y: 100, opacity: 0 },
        {
          y: 0,
          opacity: 1,
          duration: 0.7,
        }
      )
      .fromTo(
        ".gallery__inner  .splide__arrows",
        { x: -100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
        }
      )
      .fromTo(
        ".gallery__inner  .splide__pagination",
        { x: 100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
        },
        "<"
      );
  }

  if (document.querySelector(".history__inner")) {
    let fadeHistory = gsap.timeline({
      scrollTrigger: {
        trigger: ".history__inner",
        start: "top+=200px bottom",
        duration: 0.7,
        ease: "cubic-bezier",
        markers: false,
      },
    });
    fadeHistory
      .fromTo(
        ".history__inner .heading-three",
        { x: -100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
          duration: 0.7,
        }
      )
      .fromTo(
        ".history__inner .splide__track",
        { y: 100, opacity: 0 },
        {
          y: 0,
          opacity: 1,
          duration: 0.7,
        }
      )
      .fromTo(
        ".history__inner .splide__arrows",
        { x: -100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
        }
      )
      .fromTo(
        ".history__inner .splide__pagination",
        { x: 100, opacity: 0 },
        {
          x: 0,
          opacity: 1,
        },
        "<"
      );
  }

  // SCROLL SECTIONS FADE END

  if (document.querySelectorAll(".hero__line.-advantages").length) {
    gsap.fromTo(
      ".hero__line.-advantages",
      { left: "100%" },
      {
        left: "50%",
        scrollTrigger: {
          trigger: ".hero-slide__inner.-advantages",
          start: "top center",
          end: "top top",
          scrub: 1.5,
          once: true,
          ease: "cubic-bezier",
        },
      }
    );
  }

  if (document.querySelectorAll(".hero__line.-map").length) {
    gsap.fromTo(
      ".hero__line.-map",
      { bottom: "100%" },
      {
        bottom: "10%",
        scrollTrigger: {
          trigger: ".hero-slide__inner.-map",
          start: "top center",
          end: "bottom center",
          scrub: 1.5,
          once: true,
          ease: "cubic-bezier",
        },
      }
    );
  }

  if (document.querySelectorAll(".hero__line.-solutions").length) {
    let getDistanceToSolutions = function () {
      return document.querySelector(".slider__top").getBoundingClientRect().left - 20;
    };
    gsap.to(".hero__line.-solutions", {
      width: getDistanceToSolutions,
      scrollTrigger: {
        trigger: ".hero-slide__inner.-solutions",
        start: "top+=200px bottom",
        end: "top top",
        ease: "cubic-bezier",
        scrub: 1.5,
        once: true,
      },
    });
  }

  if (document.querySelector("form")) {
    let innerForm = document.querySelector("form").innerHTML;
  }

  validate.init({
    fieldClass: "-error",
    messageValueMissing: "Заполните это поле, пожалуйста",
    messageTypeMismatchEmail: "Формат поля Email: yourname@example.com",
    messagePatternMismatch: "Неверный формат",
    disableSubmit: true,
    onSubmit: function (form, fields) {
      fetch("/wp-content/themes/factory16/send.php", {
        method: "post",
        body: new FormData(form), //fields,
      })
        .then((response) => {
          response.json();
        })
        .then((data) => {
          let innerForm = document.querySelector("form").innerHTML;
          document.querySelector(
            "form"
          ).innerHTML = `<h5 class="heading-five">Спасибо за ваше сообщение!<br />Мы ответим вам в ближайшее время.</h5><h5 class="heading-five"><a href="#" id="newFormButton" class="form__link">Отправить новое сообщение<img src="../../img/form-arrow.svg" alt="" class="form__arrow" /></a></h5>`;
          let buttonFormNew = document.getElementById("newFormButton");
          buttonFormNew.addEventListener("click", function (event) {
            event.preventDefault();
            document.querySelector("form").innerHTML = innerForm;
          });
          // console.log("Success:", data);
        })
        .catch((error) => {
          let innerForm = document.querySelector("form").innerHTML;
          document.querySelector(
            "form"
          ).innerHTML = `<h5 class="heading-five">Ошибка: ${error}</h5><h5 class="heading-five"><a href="#" id="newFormButton" class="form__link">Отправить новое сообщение<img src="../../img/form-arrow.svg" alt="" class="form__arrow" /></a></h5>`;
          let buttonFormNew = document.getElementById("newFormButton");
          buttonFormNew.addEventListener("click", function (event) {
            event.preventDefault();
            document.querySelector("form").innerHTML = innerForm;
          });
          // console.error("Error:", error);
        });
    },
  });
});
